#!/usr/bin/env python3

from aws_cdk import core

from stacks.base_stack import BaseStack

app = core.App()
tags = app.node.try_get_context("tags")


BaseStack(app, "paff-base",
          env=core.Environment(
              region='eu-west-2',
              account='779679102014'
          ),
          tags=tags,
          stack_name="paff-base"
          )

app.synth()