import logging

from aws_cdk import (
    core,
    aws_apigateway as _apigw,
    aws_lambda as _lambda,
)
from botocore.exceptions import ClientError

class BaseStack(core.Stack):

    def __init__(self, scope: core.Construct, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # accountId = core.Aws.ACCOUNT_ID
        # region = core.Aws.REGION;

        try:

            # LAMBDAS # -----------------------------------------------------------------------------------------------
            self.lambdas = []


            request_handler_function = _lambda.Function(
                self, 'RequestHandler',
                runtime=_lambda.Runtime.PYTHON_3_7,
                code=_lambda.Code.asset(),
                function_name='valkyrie-afms-request-handler',
                handler='lambda_handler.handler',
                timeout=Duration.minutes(1),
                memory_size=128
            )
            self.lambdas.append(request_handler_function)


            # APIGATEWAY # --------------------------------------------------------------------------------------------
            request_gateway = _apigw.RestApi(self, 'Paff-Endpoint')


        except ClientError as e:
            logging.error(e)
